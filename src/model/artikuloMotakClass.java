package model;

public class artikuloMotakClass extends Connect{

	protected int idMota;
	protected String izena;

	public artikuloMotakClass(int idMota, String izena) {
		super();
		this.idMota = idMota;
		this.izena = izena;
	}

	public artikuloMotakClass() {
		super();
	}

	public int getIdMota() {
		return idMota;
	}

	public void setIdMota(int idMota) {
		this.idMota = idMota;
	}

	public String getIzena() {
		return izena;
	}

	public void setIzena(String izena) {
		this.izena = izena;
	}

}
