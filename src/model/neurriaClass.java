package model;

public class neurriaClass extends Connect{

	// PARAMETROAK

	protected int idNeurria;
	protected String Izena;

	// KONSTRUKTOREAK

	public neurriaClass(int idNeurria, String izena) {
		super();
		this.idNeurria = idNeurria;
		Izena = izena;
	}

	// GETTER ETA SETTERRRAK

	public neurriaClass() {
		super();
	}

	public int getIdNeurria() {
		return idNeurria;
	}

	public void setIdNeurria(int idNeurria) {
		this.idNeurria = idNeurria;
	}

	public String getIzena() {
		return Izena;
	}

	public void setIzena(String izena) {
		Izena = izena;
	}

}
