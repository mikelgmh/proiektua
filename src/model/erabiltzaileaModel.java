package model;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class erabiltzaileaModel extends erabiltzaileaClass {

	// Super konstruktorea

	public erabiltzaileaModel() {
		super();
	}

	public erabiltzaileaModel(int idErabiltzaile, String izena, String erabiltzailea, String abizena, String pasahitza,
			String dni, String tzenbakia,String rango) {
		super(idErabiltzaile, izena, erabiltzailea, abizena, pasahitza, dni, tzenbakia,rango);
	}

	// METODOAK

	// Pasahitza enkriptatzeko metodoa.
	public String hashPassword(String plainTextPassword) {
		String securePasahitza = BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
		return securePasahitza;
	}

	// Sartutako pasahitza eta datubaseko hash pasahitza konparatzen ditu
	public boolean checkPass(String plainPassword, String erabiltzailea) {
		boolean login = false;
		String hashedPassword = hartuPasahitza(erabiltzailea);
		if (BCrypt.checkpw(plainPassword, hashedPassword)) {
			login = true;
		}
		return login;
	}

	// Esandako erabiltzailearen hash pasahitza hartzen du datubasetik
	public String hartuPasahitza(String perabiltzailea) {
		String SQL = "CALL 	spCogerContrasena(?,?)";
		String emaitza = "";
		try (Connection connection = Connect.getConnection(); CallableStatement cs = connection.prepareCall(SQL)) {
			cs.setString(1, perabiltzailea);
			cs.execute();
			emaitza = cs.getString(2);
		} catch (Exception e) {
			System.out.println(e);
		}
		return emaitza;
	}

	// Erregistratu baino lehen erabiltzailea existitzen den ala ez konprobatu
	public boolean erabiltzaileaKonprobatu(String perabiltzailea) {
		boolean aurkituta = false;

		String SQL = "CALL 	spComprobarSiExisteUsuario(?,?)";
		try (Connection connection = Connect.getConnection(); CallableStatement cs = connection.prepareCall(SQL)) {
			cs.setString(1, perabiltzailea);
			cs.execute();
			String emaitza = cs.getString(2);
			if (emaitza.equalsIgnoreCase(perabiltzailea)) {
				aurkituta = true;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return aurkituta;
	}

	// Bezero bat erregistratzen du
	public boolean erregistratu(String pizena, String pabizena, String perabiltzailea, String pnan, String pzenbakia,
			String ppasahitza) {
		boolean amaituta = false;
		String pasahitzaEnkriptatuta = "";
		// Pasahitza enkriptatzen da hash moduan
		pasahitzaEnkriptatuta = hashPassword(ppasahitza);

		// Prozedura hasten da
		String SQL = "CALL 	spRegistrarUsuario(?,?,?,?,?,?)";
		try (Connection connection = Connect.getConnection(); CallableStatement cs = connection.prepareCall(SQL)) {
			cs.setString(1, pizena);
			cs.setString(2, pabizena);
			cs.setString(3, perabiltzailea);
			cs.setString(4, pnan);
			cs.setString(5, pzenbakia);
			cs.setString(6, pasahitzaEnkriptatuta);
			cs.execute();
			amaituta = true;
		} catch (Exception e) {
			System.out.println(e);
		}
		return amaituta;
	}
	public String HartuRango(String perabiltzailea) {
		String SQL = "CALL 	spObtenerRango(?,?)";
		String rango="";
		try (Connection connection = Connect.getConnection(); CallableStatement cs = connection.prepareCall(SQL)) {
			cs.setString(1, perabiltzailea);
			cs.execute();
rango=cs.getString(2);
		} catch (Exception e) {
			System.out.println(e);
		}
		return rango;
	}

	public ArrayList<erabiltzaileaClass> getErabiltzaileak() throws SQLException {
		ArrayList<erabiltzaileaClass> erab_guztiak = new ArrayList<erabiltzaileaClass>();
		
		Statement st = this.getConnection().createStatement(); // the connection variable
		ResultSet rs = st.executeQuery("CALL spVerUsuarios()");

		while (rs.next()) {
			int id = Integer.parseInt((rs.getString(1)));
			String izena = (rs.getString(2));
			String Apellido = (rs.getString(3));
			String dni = (rs.getString(4));
			String telefono = (rs.getString(5));
			String usuario = (rs.getString(6));
			String rango = (rs.getString(8));
			
			erabiltzaileaClass erabiltzaile01 = new erabiltzaileaClass();
			erabiltzaile01.idErabiltzaile = id;
			erabiltzaile01.izena = izena;
			erabiltzaile01.abizena = Apellido;
			erabiltzaile01.dni = dni;
			erabiltzaile01.tzenbakia = telefono;
			erabiltzaile01.erabiltzailea = usuario;
			erabiltzaile01.rango = rango;

			erab_guztiak.add(erabiltzaile01);

		}
		return erab_guztiak;
	
}
	public boolean DeleteUser(int idUser)
	  {
	    boolean deletion=false;
	    String SQL = "CALL   spDeleteUser(?)";
	    try (Connection connection = Connect.getConnection(); CallableStatement cs = connection.prepareCall(SQL)) {
	      cs.setInt(1, idUser);
	      cs.execute();
	      deletion = true;
	    } catch (Exception e) {
	      System.out.println(e);
	    }
	    return deletion;
	  }

	
}
