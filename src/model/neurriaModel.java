package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class neurriaModel extends neurriaClass {

	// Super konstruktoreak

	public neurriaModel() {
		super();
	}

	public neurriaModel(int idNeurria, String izena) {
		super(idNeurria, izena);
	}

	public ArrayList<neurriaClass> GetNeurriak() throws SQLException {
		ArrayList<neurriaClass> motak = new ArrayList<neurriaClass>();

		Statement st = this.getConnection().createStatement(); // the connection variable
		ResultSet rs = st.executeQuery("CALL spObetenerTallas()");

		while (rs.next()) {
			int id = Integer.parseInt((rs.getString(1)));
			String izena = (rs.getString(2));
			neurriaClass ola = new neurriaClass();
			ola.idNeurria = id;
			ola.Izena = izena;

			motak.add(ola);

		}
		return motak;
	}

}
