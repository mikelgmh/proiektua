<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Document</title>
<link rel="stylesheet" type="text/css" href="../css/artikuloberria.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<script src="../js/bootstrap.min.js"></script>
</head>

<body>
	<%
		ArrayList<artikuloMotakClass> motak = (ArrayList<artikuloMotakClass>) request.getAttribute("motak");
		ArrayList<neurriaClass> neurriak = (ArrayList<neurriaClass>) request.getAttribute("neurriak");
	%>

	<video autoplay="autoplay" loop="loop" muted="muted"
		poster="screenshot.jpg" id="background"> <source
		src="../videos/videoplayback.mp4"></video>

	<div class="card card-container">

		<label for="card-container">Artikulo berria</label>
		<p id="profile-name" class="profile-name-card"></p>
		<form class="form-signin"
			action="../controller/artikuloBerriaController.jsp">
			<span id="reauth-email" class="reauth-email"></span> <label
				for="inputIzena">Izena</label> <input type="text" id="inputIzena"
				name="izena" class="form-control" placeholder="Izena" maxlength="20"
				required="required" autofocus="autofocus"> <label
				for="inputAbizena">Deskribapena</label>
			<textarea name="deskribapena" class="form-control" rows="5" id="comment"></textarea>

			<br>

			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<label class="input-group-text" for="inputGroupSelect01">Mota</label>
				</div>
				<select class="custom-select" id="inputGroupSelect01" name="aukeramotak">
					<%
						for (int i = 0; i < motak.size(); i++) {
					%>
					<option  value="<%=motak.get(i).getIdMota()%>"><%=motak.get(i).getIzena()%></option>
					<%
						}
					%>
				</select>
			</div>

			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<label class="input-group-text" for="inputGroupSelect01">Neurria</label>
				</div>
				<select class="custom-select" id="inputGroupSelect01" name="aukeraneurria">
					<%
						for (int i = 0; i < neurriak.size(); i++) {
					%>
					<option 
						value="<%=neurriak.get(i).getIdNeurria()%>"><%=neurriak.get(i).getIzena()%></option>
					<%
						}
					%>
				</select>
			</div>

			<label for="inputPrezioa">Prezioa</label> <input type="number"
				id="inputPrezioa" name="prezioa" class="form-control"
				placeholder="Adb.: 9,99" min="0.00" max="10000.00" step="0.01"
				required="required" autofocus="autofocus"> <br>
				
				<input type="hidden" id="oculto" name="aukera" value="2">

			<button class="btn btn-lg btn-success btn-block btn-signin "
				type="submit ">Artikulo berria</button>
		</form>
		<!-- /form -->
	</div>

</body>
</html>
