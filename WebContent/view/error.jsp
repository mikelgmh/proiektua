<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Dark Error Page Responsive Widget Template| Home ::
	W3layouts</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Refresh"
	content="10;URL=<%=request.getAttribute("orrialdera")%>">
<meta name="keywords"
	content="Dark Error Page Responsive Widget Template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!-- Meta tag Keywords -->
<!-- css files -->
<link href="../css/error.css" rel="stylesheet" type="text/css" media="all">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"
	media="all">
<!-- online-fonts -->
<link
	href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	rel="stylesheet">
<!--//online-fonts -->
<body>
	<div class="header">
		<h1>Arazo bat egon da</h1>
	</div>
	<div class="w3-main">
		<div class="agile-info">
			<h2></h2>
			<h3>oops!</h3>
			<br>
			<p>
				<%
					out.print(request.getAttribute("erroremezua"));
				%>
				<br>
			</p>


			<a href="<%=request.getAttribute("orrialdera")%>">Atzera joan edo 10 segundu itxaron</a>
		</div>
	</div>


</body>
</html>