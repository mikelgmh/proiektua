<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ page import="model.*"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	if (session.getAttribute("erabiltzailea") == null ) // checks if there's an active session
	{
		response.sendRedirect("../index.jsp");
	}
	ArrayList<artikuloaClass> artikuloak = (ArrayList<artikuloaClass>) request.getAttribute("artikuloak");
	ArrayList<artikuloMotakClass> motak = (ArrayList<artikuloMotakClass>) request.getAttribute("motak");
	ArrayList<neurriaClass> neurriak = (ArrayList<neurriaClass>) request.getAttribute("neurriak");

%>
<head>
<link rel="stylesheet" href="../css/bootstrap.min.css">
<script src="../js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col">Izena</th>
      <th scope="col">Deskribapena</th>
      <th scope="col">Mota</th>
      <th scope="col">Prezioa</th>
      <th scope="col">Neurria</th>
      <th scope="col">Stock</th>
      <th scope="col">Aldatu</th>
    </tr>
  </thead>
  <tbody>
  
  <%
  for(int i=0;i<artikuloak.size();i++){
	 %>
	 <form action="../controller/updateController.jsp">
	     <tr>
      <th scope="row"><%=artikuloak.get(i).getIdArtikuloa() %></th>
      <td><input type="text" id="inputIzena"
				name="izena" class="form-control" value="<%=artikuloak.get(i).getIzena() %>" maxlength="20"
				autofocus="autofocus"> </td>
      <td><input type="text" id="inputIzena"
				name="deskribapena" class="form-control" value="<%=artikuloak.get(i).getDeskribapena() %>" maxlength="20"
				autofocus="autofocus"> </td>
      <td>			<div class="input-group mb-3">
				<div class="input-group-prepend">
				</div>
				<select class="custom-select" id="inputGroupSelect01" name="aukeramotak">
					<%
					int idMotaGorde=0;
						for (int x = 0; x < motak.size(); x++) {
							// If para que no se repita el nombre en el option
							if(motak.get(x).getIzena().equalsIgnoreCase(artikuloak.get(i).getMota()) == false){
								
							
								%>
								<option  value="<%=motak.get(x).getIdMota()%>"><%=motak.get(x).getIzena()%></option>
								<%
							}else{
								idMotaGorde = motak.get(x).getIdMota();
							}

						}
					%>
									<option selected  value="<%=idMotaGorde%>"><%=artikuloak.get(i).getMota()%></option>
				</select>
			</div> </td>
      <td><input type="text" id="inputIzena"
				name="prezioa" class="form-control" value="<%=artikuloak.get(i).getPrezioa() %>" maxlength="20"
				autofocus="autofocus"> </td>
				
         <td>			<div class="input-group mb-3">
				<div class="input-group-prepend">
				</div>
				<select class="custom-select" id="inputGroupSelect01" name="aukeraneurria">
					<%
					int idNeurriaGorde=0;
						for (int x = 0; x < motak.size(); x++) {
							// If para que no se repita el nombre en el option
							if(neurriak.get(x).getIzena().equalsIgnoreCase(artikuloak.get(i).getNeurria()) == false){
								
							
								%>
								<option  value="<%=neurriak.get(x).getIdNeurria()%>"><%=neurriak.get(x).getIzena()%></option>
								<%
							}else{
								idNeurriaGorde = neurriak.get(x).getIdNeurria();
							}

						}
					%>
									<option selected  value="<%=idNeurriaGorde%>"><%=artikuloak.get(i).getNeurria()%></option>
									<input type="hidden" id="thisField" name="artikuloid" value="<%=artikuloak.get(i).getIdArtikuloa()%>">
				</select>
			</div> </td>
      <td><input type="text" id="inputIzena"
				name="stock" class="form-control" value="<%=artikuloak.get(i).getStock()%>" maxlength="20"
				autofocus="autofocus"> </td>
				<input type="hidden" id="thisField" name="aukera" value="1">
<td><button type="submit" name="botoia" class="btn">Aldatu</button></td>
    </tr>
    </form>
    
	 <% 
  }
  %>
  


  </tbody>
</table>
</body>
</html>