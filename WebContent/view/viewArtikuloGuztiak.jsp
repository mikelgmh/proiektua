<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="model.*"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	if (session.getAttribute("erabiltzailea") == null ) // checks if there's an active session
	{
		response.sendRedirect("../index.jsp");
	}
	ArrayList<artikuloaClass> artikuloak = (ArrayList<artikuloaClass>) request.getAttribute("artikuloak");

%>
<head>
<link rel="stylesheet" href="../css/bootstrap.min.css">
<script src="../js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>


<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col">Izena</th>
      <th scope="col">Deskribapena</th>
      <th scope="col">Mota</th>
      <th scope="col">Prezioa</th>
      <th scope="col">Neurria</th>
      <th scope="col">Stock</th>
    </tr>
  </thead>
  <tbody>
  
  <%
  for(int i=0;i<artikuloak.size();i++){
	 %>
	 <form action="../controller/deleteController.jsp">
	     <tr>
      <th scope="row"><%=artikuloak.get(i).getIdArtikuloa() %></th>
      <td><%=artikuloak.get(i).getIzena() %></td>
      <td><%=artikuloak.get(i).getDeskribapena() %></td>
      <td><%=artikuloak.get(i).getMota() %></td>
      <td><%=artikuloak.get(i).getPrezioa() %></td>
      <td><%=artikuloak.get(i).getNeurria() %></td>
      <td><%=artikuloak.get(i).getStock()%></td>
    </tr>
    </form>
    
	 <% 
  }
  %>
  


  </tbody>
</table>


</body>
</html>