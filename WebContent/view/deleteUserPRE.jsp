<%@ page import="model.*"%>
<%@ page import="java.util.*"%>
<!doctype html>

<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/style.css">

<title></title>
</head>
<body>
	<%
		ArrayList<erabiltzaileaClass> AllErab = (ArrayList<erabiltzaileaClass>) request.getAttribute("erabiltzaileak");
	%>
	<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col">Izena</th>
      <th scope="col">Deskribapena</th>
      <th scope="col">Mota</th>
      <th scope="col">Prezioa</th>
      <th scope="col">Neurria</th>
      <th scope="col">Stock</th>
      <th scope="col">Aldatu</th>
    </tr>
  </thead>
  <tbody>
  
  <%
  for(int i=0;i<AllErab.size();i++){
	 %>
	 <form action="../controller/deleteController.jsp">
	     <tr>
      <th scope="row"><input type="hidden" id="thisField" name="idErab" value="<%=AllErab.get(i).getIdErabiltzaile() %>"><%=AllErab.get(i).getIdErabiltzaile() %></th>

      <td><%=AllErab.get(i).getIzena() %> </td>
      <td><%=AllErab.get(i).getAbizena() %> </td>
      <td><%=AllErab.get(i).getDni() %> </td>
      <td><%=AllErab.get(i).getTzenbakia() %> </td>
      <td><%=AllErab.get(i).getErabiltzailea() %> </td>
      <td><%=AllErab.get(i).getRango() %> </td>
      <input type="hidden" id="thisField" name="aukera" value="1">
<td><button type="submit" name="botoia" class="btn" >Ezabatu</button></td>
    </tr>
    </form>
    
	 <% 
  }
  %>
  


  </tbody>
</table>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script src="../js/bootstrap.min.js"></script>
</body>
</html>