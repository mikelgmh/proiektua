<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>
<%@ page import="java.util.*"%>

<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<%
		int aukera = Integer.parseInt(request.getParameter("aukera"));
		switch (aukera) {
		case 1:
			session.invalidate();
			response.sendRedirect("../index.jsp");
			break;
		case 2: // Artikulo bat aldatu PREVIEW
			// Hartu artikuloak
			artikuloaModel artikuloa01 = new artikuloaModel();
			ArrayList<artikuloaClass> art_guztiak = new ArrayList<artikuloaClass>();
			art_guztiak = artikuloa01.getArtikuloak();

			// Hartu motak
			artikuloMotakModel motak01 = new artikuloMotakModel();
			ArrayList<artikuloMotakClass> mota_guztiak = new ArrayList<artikuloMotakClass>();
			mota_guztiak = motak01.Getmotak();

			// Hartu neurriak
			neurriaModel neurria01 = new neurriaModel();
			ArrayList<neurriaClass> neurri_guztiak = new ArrayList<neurriaClass>();
			neurri_guztiak = neurria01.GetNeurriak();

			// Bidali neurrien arraylist-a
			request.setAttribute("neurriak", neurri_guztiak);

			// Bidali artikuloen arraylist-a
			request.setAttribute("artikuloak", art_guztiak);

			// Bidali moten arraylist-a
			request.setAttribute("motak", mota_guztiak);
			request.getRequestDispatcher("../view/viewArtikuloaAldatu.jsp").forward(request, response);
			break;
		case 3: // Erabiltzaile bat ezabatu PREVIEW
			erabiltzaileaModel newmodel = new erabiltzaileaModel();
			ArrayList<erabiltzaileaClass> erabiltzaileak = new ArrayList<erabiltzaileaClass>();

			erabiltzaileak = newmodel.getErabiltzaileak();
			request.setAttribute("erabiltzaileak", erabiltzaileak);
			request.getRequestDispatcher("../view/deleteUserPRE.jsp").forward(request, response);
			break;
		case 4: // Artikulo bat ezabatu PREVIEW
			// Hartu artikuloak
			artikuloaModel artikuloa02 = new artikuloaModel();
			ArrayList<artikuloaClass> art_guztiak1 = new ArrayList<artikuloaClass>();
			art_guztiak1 = artikuloa02.getArtikuloak();

			// Hartu motak
			artikuloMotakModel motak02 = new artikuloMotakModel();
			ArrayList<artikuloMotakClass> mota_guztiak2 = new ArrayList<artikuloMotakClass>();
			mota_guztiak2 = motak02.Getmotak();

			// Hartu neurriak
			neurriaModel neurria02 = new neurriaModel();
			ArrayList<neurriaClass> neurri_guztiak2 = new ArrayList<neurriaClass>();
			neurri_guztiak2 = neurria02.GetNeurriak();

			// Bidali neurrien arraylist-a
			request.setAttribute("neurriak", neurri_guztiak2);

			// Bidali artikuloen arraylist-a
			request.setAttribute("artikuloak", art_guztiak1);

			// Bidali moten arraylist-a
			request.setAttribute("motak", mota_guztiak2);
			request.getRequestDispatcher("../view/viewArtikuloaEzabatu.jsp").forward(request, response);
			break;
		case 5: // Artikulo guztiak ikusi
			// Hartu artikuloak
			artikuloaModel artikuloa03 = new artikuloaModel();
			ArrayList<artikuloaClass> art_guztiak3 = new ArrayList<artikuloaClass>();
			art_guztiak3 = artikuloa03.getArtikuloak();
			
			// Bidali artikuloen arraylist-a
			request.setAttribute("artikuloak", art_guztiak3);
			request.getRequestDispatcher("../view/viewArtikuloGuztiak.jsp").forward(request, response);
			break;
		default:
			System.out.println("error");
			break;
		}
	%>
</body>
</html>