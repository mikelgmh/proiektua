<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="model.*"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%
int auk=Integer.parseInt(request.getParameter("aukera"));
switch (auk) {
case 1://erabiltzailea ezabatu
erabiltzaileaModel myModel= new erabiltzaileaModel();
int id=Integer.parseInt(request.getParameter("idErab"));
boolean aldatuta = myModel.DeleteUser(id);

if (aldatuta == true) {
	String confirm = "Erabiltzailea ezabatu da.";
	request.setAttribute("confirmmezua", confirm);

	// Errorea eta gero zein orrialdera joango den
	String orrialdera = "../index.jsp";
	request.setAttribute("orrialdera", orrialdera);
	request.getRequestDispatcher("../view/confirm.jsp").forward(request, response);
}else{
	//Errore mezua bidaltzen du
	String errorea = "Ezin da erabiltzailea ezabatu";
	request.setAttribute("erroremezua", errorea);

	// Errorea eta gero zein orrialdera joango den
	String orrialdera = "../index.jsp";
	request.setAttribute("orrialdera", orrialdera);
	request.getRequestDispatcher("../view/error.jsp").forward(request, response);
}
break;
case 2: // Artikulo bat ezabatu
int idArt = Integer.parseInt(request.getParameter("idArtikuloa"));
	artikuloaModel artikuloaEzabatu = new artikuloaModel();
	boolean ArtEzabatuta = artikuloaEzabatu.artikuloaEzabatu(idArt);
	
	if (ArtEzabatuta == true) {
		String confirm = "Artikuloa ezabatu da.";
		request.setAttribute("confirmmezua", confirm);

		// Errorea eta gero zein orrialdera joango den
		String orrialdera = "../index.jsp";
		request.setAttribute("orrialdera", orrialdera);
		request.getRequestDispatcher("../view/confirm.jsp").forward(request, response);
	}else{
		//Errore mezua bidaltzen du
		String errorea = "Ezin da artikuloa ezabatu";
		request.setAttribute("erroremezua", errorea);

		// Errorea eta gero zein orrialdera joango den
		String orrialdera = "../index.jsp";
		request.setAttribute("orrialdera", orrialdera);
		request.getRequestDispatcher("../view/error.jsp").forward(request, response);
	}
	
	break;


}

%>

</body>
</html>