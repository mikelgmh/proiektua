<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		int aukera = Integer.parseInt(request.getParameter("aukera"));
		switch (aukera) {
		case 1:
			if (session.getAttribute("erabiltzailea") == null || session.getAttribute("rango").equals(0)) // checks if there's an active session
			{
				response.sendRedirect("../index.jsp");
			} else {
				// Artikulo moten array-a prestatzen

				artikuloMotakModel artikuloa = new artikuloMotakModel();
				ArrayList<artikuloMotakClass> motak = new ArrayList<artikuloMotakClass>();
				motak = artikuloa.Getmotak();
				request.setAttribute("motak", motak);

				// Neurrien array-a prestatzen

				neurriaModel neurria = new neurriaModel();
				ArrayList<neurriaClass> neurriak = new ArrayList<neurriaClass>();
				neurriak = neurria.GetNeurriak();
				request.setAttribute("neurriak", neurriak);
				request.getRequestDispatcher("../view/artikuloberria.jsp").forward(request, response);

			}
			break;
		case 2:
			if (session.getAttribute("rango").equals(0)) {
				response.sendRedirect("../index.jsp");
			} else {

				int mota = Integer.parseInt(request.getParameter("aukeramotak"));
				int neurria = Integer.parseInt(request.getParameter("aukeraneurria"));
				String izena = request.getParameter("izena");
				String deskribapena = request.getParameter("deskribapena");
				double prezioa = Double.parseDouble(request.getParameter("prezioa"));

				artikuloaModel artikulo01 = new artikuloaModel();
				boolean amaituta = artikulo01.artikuloBerria(mota, neurria, izena, deskribapena, prezioa);
				if (amaituta != true) {

					//Errore mezua bidaltzen du
					String errorea = "Ezin da artikuloa sartu";
					request.setAttribute("erroremezua", errorea);

					// Errorea eta gero zein orrialdera joango den
					String orrialdera = "../homeController.jsp";
					request.setAttribute("orrialdera", orrialdera);
					request.getRequestDispatcher("../view/error.jsp").forward(request, response);
				} else {

					String confirm = "Artikuloa datu basean sartu da";
					request.setAttribute("confirmmezua", confirm);

					// Errorea eta gero zein orrialdera joango den
					String orrialdera = "../index.jsp";
					request.setAttribute("orrialdera", orrialdera);
					request.getRequestDispatcher("../view/confirm.jsp").forward(request, response);
				}

			}
			break;
		case 3:
			break;
		case 4:
			break;
		default:
			System.out.println("error");
			break;
		}
	%>
</body>
</html>