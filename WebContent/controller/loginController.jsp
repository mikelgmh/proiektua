<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Kontroladorea</title>
</head>
<body>
	<%
		erabiltzaileaModel berria = new erabiltzaileaModel();
		// Formulariotik datuak hartu eta aldagaietan gorde
		String erabiltzailea = request.getParameter("erabiltzailea");
		String pasahitza = request.getParameter("pasahitza");

		boolean login = berria.checkPass(pasahitza, erabiltzailea);
		if (login == true) {
			
			session.setAttribute("rango", berria.HartuRango(erabiltzailea));
			session.setAttribute("erabiltzailea", erabiltzailea);
			response.sendRedirect("homeController.jsp");
		} else {
			String errorea = "Erabiltzailea edo pashitza txarto";
			request.setAttribute("erroremezua", errorea);

			// Errorea eta gero zein orrialdera joango den
			String orrialdera = "../index.jsp";
			request.setAttribute("orrialdera", orrialdera);
			request.getRequestDispatcher("../view/error.jsp").forward(request, response);
		}
	%>

</body>
</html>