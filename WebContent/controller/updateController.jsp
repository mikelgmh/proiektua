<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="model.*"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		int aukera = Integer.parseInt(request.getParameter("aukera"));
		switch (aukera) {
		case 1: // UPDATE Artikuloa
			String izena = request.getParameter("izena");
			int id = Integer.parseInt(request.getParameter("artikuloid"));
			String deskribapena = request.getParameter("deskribapena");
			int mota = Integer.parseInt(request.getParameter("aukeramotak"));
			double prezioa = Double.parseDouble(request.getParameter("prezioa"));
			int neurria = Integer.parseInt(request.getParameter("aukeraneurria"));
			int stock = Integer.parseInt(request.getParameter("stock"));

			boolean aldatuta;
			//ArtikuloModel objetua sortu
			artikuloaModel artikuloa01 = new artikuloaModel();

			// Artikuloa aldatzeko metodoa deitu
			aldatuta = artikuloa01.artikuloaAldatu(mota, neurria, izena, deskribapena, prezioa, id, stock);
			if (aldatuta == true) {
				String confirm = "Artikuloa aldatu da.";
				request.setAttribute("confirmmezua", confirm);

				// Errorea eta gero zein orrialdera joango den
				String orrialdera = "../index.jsp";
				request.setAttribute("orrialdera", orrialdera);
				request.getRequestDispatcher("../view/confirm.jsp").forward(request, response);
			}else{
				//Errore mezua bidaltzen du
				String errorea = "Ezin da artikuloa aldatu";
				request.setAttribute("erroremezua", errorea);

				// Errorea eta gero zein orrialdera joango den
				String orrialdera = "../index.jsp";
				request.setAttribute("orrialdera", orrialdera);
				request.getRequestDispatcher("../view/error.jsp").forward(request, response);
			}

			break;
		case 2:

			break;
		case 3:

		case 4:
			break;
		default:
			System.out.println("error");
			break;
		}
	%>
</body>
</html>